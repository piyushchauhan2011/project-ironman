'use strict';

var React = require('react-native');
var Icon = require('FAKIconImage');
var SearchPageDetails = require('./search_page_details');
var ExplorePage = require('./explore_page');
var fonts = require('./fonts/fonts_styles');

var {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableWithoutFeedback,
} = React;

var styles = StyleSheet.create({
	navigationBar: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 20,
    paddingBottom: 10,
    backgroundColor: '#FAFAFA',
    borderBottomColor: '#CCC',
    borderBottomWidth: 1,
    paddingTop: 25,
  },
  hamburgerOpen: {
    flex: 2,
    alignItems: 'center',
    alignSelf: 'center',
  },
  docitSearch: {
    flex: 10,
  },
  dsInput: {
    height: 30,
    fontSize: 16,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    borderColor: '#CCC',
    borderRadius: 2,
    borderWidth: 2,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
});

var Header = React.createClass({
  getInitialState: function() {
    return {
      something: 'hello',
      other: false,
      headerType: 'static',
    };
  },
  componentDidMount: function() {
    global.header = this;
  },
  loadSearchPage: function() {
    // console.log(this.refs);
    this.setState({
      headerType: 'search',
    });
    global.navigator.push({
      name: 'SearchPageDetails',
      component: SearchPageDetails,
    });
  },
  takeBack: function() {
    var rootRoute = global.navigator.pop();
    global.items -= 1;
    // console.log(global.navigator);
    if(global.items == 1) {
      this.setState({ other: false });
      console.log(this.refs);
    }
  },
  populateSearch: function(text) {
    global.searchPage.populateAndSearchResults(text);
  },
  render: function() {
    var searchHeader = (
      <View style={styles.navigationBar}>
        <View style={styles.hamburgerOpen}>
          <TouchableWithoutFeedback
            style={styles.hamburgerIcon}
            onPress={this.takeBack}>
            <Icon
              name='fontawesome|arrow-left'
              size={25}
              color='#009D3E'
              style={{height:25,width:25}}/>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.docitSearch}>
          <TextInput ref='search' style={styles.dsInput}
            onChangeText={this.populateSearch}
            enablesReturnKeyAutomatically={true}
            placeholder='Search by #tags, name...'/>
        </View>
      </View>
    );
    var navigableHeader = (
      <View style={styles.navigationBar}>
        <View style={styles.hamburgerOpen}>
          <TouchableWithoutFeedback
            style={styles.hamburgerIcon}
            onPress={this.takeBack}>
            <Icon
              name='fontawesome|arrow-left'
              size={25}
              color='#009D3E'
              style={{height:25,width:25}}/>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.docitSearch}>
          <View style={{ alignItems: 'center', alignSelf: 'center', marginLeft: -50, }}>
            <Text style={[fonts.lato, { fontSize: 24 }]}>Docit</Text>
          </View>
        </View>
      </View>      
    );
    var staticHeader = (
      <View style={styles.navigationBar}>
        <View style={styles.hamburgerOpen}>
          <TouchableWithoutFeedback
            style={styles.hamburgerIcon}
            onPress={this.props.menuActions.toggle}>
            <Icon
              name='fontawesome|bars'
              size={25}
              color='#009D3E'
              style={{height:25,width:25}}/>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.docitSearch}>
          <TextInput ref='staticHeader' style={styles.dsInput}
            onFocus={this.loadSearchPage}
            onChangeText={this.populateSearch}
            placeholder='#hashtags, business, professional, location...' />
        </View>
      </View>
    );

    var fragment;
    // if(this.state.headerType === 'search') { fragment=searchHeader; }
    // if(this.state.headerType === 'static') { console.log(this.refs); fragment=staticHeader; }

    if(this.state.other) { fragment = navigableHeader; }
    else { fragment = staticHeader; }

    return (
      <View>
      {fragment}
      </View>
    );
  }
});

module.exports = Header;