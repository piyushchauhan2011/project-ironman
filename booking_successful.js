'use strict';

var React = require('react-native');
var Icon = require('FAKIconImage');
var moment = require('moment');
var Appointments = require('./appointments');

var {
	StyleSheet,
  Text,
  View,
  TouchableHighlight,
} = React;

var styles = StyleSheet.create({
  bookingSuccess: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#EEE',
    padding: 10,
  },
  thanksMessage: {
    flexDirection: 'column',
    padding: 10,
    paddingLeft: 20,
  },
  professionalDetails: {
    flexDirection: 'column',
    padding: 10,
    borderColor: '#CCC',
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderRadius: 3,
    margin: 10,
    shadowColor: '#DDD',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 10,
  },
  pdRow: {
    flexDirection: 'row',
    marginBottom: 5,
    marginTop: 5,
  },
  pdIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  phlIcon: {
    flex: 1,
    height: 25,
    width: 25,
    padding: 5,
  },
  pdText: {
    flex: 3,
  },
  pdlTxt: {
    fontSize: 16,
    paddingTop: 3,
  },
  tmFont: {
    fontSize: 16,
  },
  tmFontLarge: {
    fontSize: 20,
  },
  doneButton: {
    flexDirection: 'column',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  button: {
    height: 36,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#009D6E',
    borderColor: '#008C5D',
    borderWidth: 1,
    borderRadius: 3,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    margin: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
});

var BookingSuccessful = React.createClass({
  getInitialState: function() {
    global.header.setState({ other: false });
    console.log(this.props.bookingDetails);
    return {};
  },
  doneBooking: function() {
    global.navigator.push({
      name: 'Appointments',
      component: Appointments,
    });
  },
  render: function() {
    return (
      <View style={styles.bookingSuccess}>
        <View style={styles.thanksMessage}>
          <Text style={[styles.tmFontLarge, {marginBottom: 10}]}>Thanks {this.props.bookingDetails.name}!</Text>
          <Text style={styles.tmFont}>Your Appointment, </Text>
        </View>
        <View style={styles.professionalDetails}>
          <View style={styles.pdRow}>
            <View style={styles.pdIcon}>
              <Icon
                name='fontawesome|user'
                size={25}
                color='#042'
                style={styles.phlIcon}/>
            </View>
            <View style={styles.pdText}>
              <Text style={styles.pdlTxt}>{this.props.bookingDetails.professional.name}</Text>
            </View>
          </View>
          <View style={styles.pdRow}>
            <View style={styles.pdIcon}>
              <Icon
                name='fontawesome|clock-o'
                size={25}
                color='#042'
                style={styles.phlIcon}/>
            </View>
            <View style={styles.pdText}>
              <Text style={styles.pdlTxt}>{this.props.bookingDetails.slotTime}</Text>
            </View>
          </View>
          <View style={styles.pdRow}>
            <View style={styles.pdIcon}>
              <Icon
                name='fontawesome|calendar'
                size={25}
                color='#042'
                style={styles.phlIcon}/>
            </View>
            <View style={styles.pdText}>
              <Text style={styles.pdlTxt}>{moment(this.props.bookingDetails.bookingDate).format('Do MMMM')} ( {moment(this.props.bookingDetails.bookingDate).fromNow()} )</Text>
            </View>
          </View>
        </View>
        <View style={styles.thanksMessage}>
          <Text style={styles.tmFont}>has been booked and confirmed.</Text>
        </View>
        <View style={styles.doneButton}>
          <TouchableHighlight style={styles.button}
            onPress={this.doneBooking} underlayColor='#009D6E'>
            <Text style={styles.buttonText}>DONE</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
});

module.exports = BookingSuccessful;