'use strict';

var React = require('react-native');
var SearchPageDetails = require('./search_page_details');
var BookingStep1 = require('./booking_step1');
var Appointments = require('./appointments');
var PictureView = require('./picture_view');
var Booking = require('./booking');
var VerificationView = require('./verification_view');
var SettingsView = require('./settings_view');
var FeedbackView = require('./feedback_view');
var BookingSuccessful = require('./booking_successful');
var EmailSignup = require('./email_signup');
var Icon = require('FAKIconImage');
var fonts = require('./fonts/fonts_styles');
var DocitStore = require('./docit_store');
DocitStore.init();

var Dimensions = require('Dimensions');
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var {
  StyleSheet,
  View,
  ScrollView,
  TextInput,
  Text,
  TouchableHighlight,
  AlertIOS,
  Image,
} = React;

var styles = StyleSheet.create({
  flowRight: {
    flexDirection: 'column',
  },
  welcome: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'stretch',
    padding: 25,
  },
  button: {
    height: 36,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#009D6E',
    borderColor: '#008C5D',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  btnClickContain: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    backgroundColor: '#009D6E',
    borderRadius: 5,
    padding: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    borderRadius: 10,
  },
  btnIcon: {
    height: 25,
    width: 25,
  },
  btnText: {
    fontSize: 18,
    color: '#FAFAFA',
    marginLeft: 10,
    marginTop: 2,
  },
  docitWelcomeLogo: {
    height: 100,
    width: screenWidth,
    padding: 10,
    marginTop: 40,
  },
  orTxt: {
    fontSize: 20,
    marginTop: 10,
    marginBottom: 10,
  },
  signInForm: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch',
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 5,
    marginBottom: 5,
  },
  txtInput: {
    height: 30,
    borderColor: '#CCC',
    borderRadius: 2,
    borderWidth: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
  },
  inputErrors: {
    borderColor: '#8F1C2D',
  },
  txtErrors: {
    paddingLeft: 5,
    paddingTop: 2,
    color: '#8F1C2D',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  successMessage: {
    paddingLeft: 5,
    paddingTop: 2,
    color: '#12341C',
  },
});

var WelcomePage = React.createClass({
  getInitialState: function() {
    global.welcomePage = true;
    global.items = 1;
    if(global.header) { global.header.setState({ other: false }); }
    return {
      email: '',
      password: '',
      loginError: '',
      loginErrorBool: false,
      user: {},
    };
  },
  onBooking: function() {
    this.props.navigator.push({
      name: 'BookingStep1',
      component: BookingStep1
    });
  },
  onExplorePressed: function() {
    global.header.setState({ other: true });
    // global.items += 1;
    this.props.navigator.push({
      name: 'SearchPageDetails',
      component: SearchPageDetails,
    });
  },
  getAppointments: function() {
    this.props.navigator.push({
      name: 'Appointments',
      component: Appointments,
    });
  },
  showAppointment: function() {
    this.props.navigator.push({
      name: 'PictureView',
      component: PictureView,
    });
  },
  booking: function() {
    this.props.navigator.push({
      name: 'Booking',
      component: Booking
    });
  },
  verificationView: function() {
    this.props.navigator.push({
      name: 'VerificationView',
      component: VerificationView
    });
  },
  settingsView: function() {
    this.props.navigator.push({
      name: 'SettingsView',
      component: SettingsView,
    });
  },
  feedbackView: function() {
    this.props.navigator.push({
      name: 'FeedbackView',
      component: FeedbackView
    });
  },
  bookingSuccessful: function() {
    this.props.navigator.push({
      name: 'BookingSuccessful',
      component: BookingSuccessful
    });
  },
  onFacebook: function() {
    fetch('http://localhost:3000/auth/facebook', {
      method: 'get'
    })
    .then((response) => {
      console.log(response.headers);
      console.log(response.text());
    });

    // use the react-native-facebook component
  },
  signupEmail: function() {
    global.header.setState({ other: true });
    this.props.navigator.push({
      name: 'EmailSignup',
      component: EmailSignup,
    });
  },
  signinEmail: function() {
    var data = JSON.stringify({
      config_name: 'default',
      email: this.state.email,
      password: this.state.password,
    });
    fetch('http://localhost:3000/auth/sign_in', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: data,
    })
      .then((response) => {
        var responseString = response.text();
        var responseText = responseString._8;
        var responseStatus = responseString._32;
        var responseHash = JSON.parse(responseText);
        if(responseHash.errors !== undefined) {
          this.setState({
            loginErrorBool: true,
            loginError: responseHash.errors[0],
          });
        }

        if(responseHash.data !== undefined) {
          this.setState({
            loginErrorBool: false,
            loginError: 'You are now logged in.',
            user: responseHash.data,
          });
          DocitStore.setLoggedUser(responseHash.data);
          DocitStore.setHeaders(response.headers);
          DocitStore.loggedIn = true;
          this.getAppointments();
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  },
  render: function() {
    var loginErrorView;
    if(this.state.loginErrorBool) {
      loginErrorView = (
        <Text style={[styles.txtErrors, fonts.lato]}>{this.state.loginError}</Text>
      );
    } else {
      loginErrorView = (
        <Text style={[styles.successMessage, fonts.lato]}>{this.state.loginError}</Text>
      );
    }

    return (
      <ScrollView style={styles.flowRight}>

        <Image
          style={styles.docitWelcomeLogo}
          resizeMode={Image.resizeMode.contain}
          source={{uri: 'http://getdocit.com/static/img/Docit_Green.png'}} />

        <View style={styles.welcome}>
          <View style={styles.signInForm}>
            {loginErrorView}
            <View style={styles.inputContainer}>
              <TextInput ref='email' style={[styles.txtInput, !this.state.loginErrorBool ? {} : styles.inputErrors, fonts.lato]} placeholder="Email..."
                onChangeText={(text) => this.setState({email: text})}/>
            </View>
            <View style={styles.inputContainer}>
              <TextInput ref='password' style={[styles.txtInput, !this.state.loginErrorBool ? {} : styles.inputErrors, fonts.lato]} placeholder="Password..." password={true}
                onChangeText={(text) => this.setState({password: text})} />
            </View>
          </View>

          <TouchableHighlight
            onPress={this.signinEmail} style={styles.btnClickContain}
            underlayColor='#042417'>
            <View
              style={styles.btnContainer}>
              <Icon
                name='fontawesome|envelope-o'
                size={25}
                color='#042'
                style={styles.btnIcon}/>
              <Text style={[styles.btnText, fonts.lato]}>Sign In</Text>
            </View>
          </TouchableHighlight>

          <Text style={[styles.orTxt, fonts.lato]}>OR</Text>
          
          <TouchableHighlight
            onPress={this.onExplorePressed} style={styles.btnClickContain}
            underlayColor='#042417'>
            <View
              style={styles.btnContainer}>
              <Icon
                name='fontawesome|search'
                size={25}
                color='#042'
                style={styles.btnIcon}/>
              <Text style={[styles.btnText, fonts.lato]}>Explore Docit</Text>
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            onPress={this.onFacebook} style={styles.btnClickContain}
            underlayColor='#042417'>
            <View
              style={styles.btnContainer}>
              <Icon
                name='fontawesome|facebook-square'
                size={25}
                color='#042'
                style={styles.btnIcon}/>
              <Text style={[styles.btnText, fonts.lato]}>Facebook</Text>
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            onPress={this.signupEmail} style={styles.btnClickContain}
            underlayColor='#042417'>
            <View
              style={styles.btnContainer}>
              <Icon
                name='fontawesome|envelope-o'
                size={25}
                color='#042'
                style={styles.btnIcon}/>
              <Text style={[styles.btnText, fonts.lato]}>Sign Up</Text>
            </View>
          </TouchableHighlight>

        </View>

      </ScrollView>
    );
  }
});

module.exports = WelcomePage;