'use strict';

var React = require('react-native');
var Icon = require('FAKIconImage');
var _ = require('lodash');
var KeyboardEvents = require('react-native-keyboardevents');
var KeyboardEventEmitter = KeyboardEvents.Emitter;
var fonts = require('./fonts/fonts_styles');

var {
  StyleSheet,
	View,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
} = React;

var styles = StyleSheet.create({
  confirmationCard: {
    flexDirection: 'column',
    backgroundColor: '#FAFAFA',
    borderRadius: 1,
    borderColor: '#CCC',
    borderWidth: 1,
    margin: 10,
    padding: 10,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 5,
    marginBottom: 5,
  },
  txtLabel: {
    fontSize: 16,
    color: '#333',
    margin: 5,
  },
  txtInput: {
    height: 30,
    borderColor: '#CCC',
    borderRadius: 2,
    borderWidth: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
  },
  bookingButtons: {
    flexDirection: 'row',
    padding: 10,
    marginTop: 10,
  },
  button: {
    height: 36,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#009D6E',
    borderColor: '#008C5D',
    borderWidth: 1,
    borderRadius: 3,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    margin: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center',
    paddingLeft: 10,
  },
  phlIcon: {
    height: 25,
    width: 25,
    marginTop: 5,
  },
  inputErrors: {
    borderColor: '#8F1C2D',
  },
  txtErrors: {
    paddingLeft: 5,
    paddingTop: 2,
    color: '#8F1C2D',
  }
});

var EmailSignup = React.createClass({
  getInitialState: function() {
    KeyboardEventEmitter.on(KeyboardEvents.KeyboardDidShowEvent, (frames) => {
      console.log('Keyboard Showing...');
      console.log(frames.end.height);
      this.setState({keyboardSpace: frames.end.height});
    });
    KeyboardEventEmitter.on(KeyboardEvents.KeyboardWillHideEvent, (frames) => {
      console.log('Keyboard Hiding');
      this.setState({keyboardSpace: 0});
    });
    return {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      password_confirmation: '',
      keyboardSpace: 0,
    };
  },
  signup: function() {
    // Good way to send json values
    var data = JSON.stringify({
        config_name: 'default',
        confirm_success_url: 'whatever',
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        email: this.state.email,
        password: this.state.password,
        password_confirmation: this.state.password_confirmation,
      });
    fetch('http://localhost:3000/auth', {
      method: 'post',
      // Bad way to send the json values
      // body: "config_name=default&first_name="+this.state.first_name+"&last_name="+this.state.last_name+"&email="+this.state.email+"&password="+this.state.password+"&password_confirmation="+this.state.password_confirmation+"&confirm_success_url=whatever",
      // Have to do to notify the server for parsing the JSON correctly.
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: data,
    })
      .then((response) => response.text())
      .then((responseText) => {
        var response = JSON.parse(responseText);
        if(response.status === 'error') {
          var error_messages = _.uniq(response.errors.full_messages);
          console.log(response.errors);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  },
  render: function() {
    return (
      <ScrollView>
        <View style={styles.confirmationCard}>
          <View style={styles.inputContainer}>
            <Text style={[styles.txtLabel, fonts.lato]}>First Name</Text>
            <TextInput style={[styles.txtInput, fonts.lato]} placeholder="Enter your first name..."
              onChangeText={(text) => this.setState({first_name: text})}/>
          </View>
          <View style={styles.inputContainer}>
            <Text style={[styles.txtLabel, fonts.lato]}>Last Name</Text>
            <TextInput style={styles.txtInput} placeholder="Enter your last name..."
              onChangeText={(text) => this.setState({last_name: text})}/>
          </View>
          <View style={styles.inputContainer}>
            <Text style={[styles.txtLabel, fonts.lato]}>Email</Text>
            <TextInput style={[styles.txtInput, fonts.lato]} placeholder="Email Address..."
              onChangeText={(text) => this.setState({email: text})}/>
          </View>
          <View style={styles.inputContainer}>
            <Text style={[styles.txtLabel, fonts.lato]}>Password</Text>
            <TextInput style={[styles.txtInput, fonts.lato]} placeholder="Password..." password={true}
              onChangeText={(text) => this.setState({password: text})} />
          </View>
          <View style={styles.inputContainer}>
            <Text style={[styles.txtLabel, fonts.lato]}>Confirm Password</Text>
            <TextInput style={[styles.txtInput, styles.inputErrors, fonts.lato]} placeholder="Confirm Password..." password={true}
              onChangeText={(text) => this.setState({password_confirmation: text})} />
            <Text style={[styles.txtErrors, fonts.lato]}>Password doesn't confirm with each other.</Text>
          </View>
          <View style={styles.bookingButtons}>
            <TouchableHighlight style={styles.button} underlayColor='#002E20' onPress={this.signup}>
            <View style={styles.buttonContainer}>
              <Icon
                name='fontawesome|user'
                size={25}
                color='#007551'
                style={[styles.phlIcon]}/>
              <Text style={[styles.buttonText, fonts.lato]}>Sign Up</Text>
            </View>
            </TouchableHighlight>
          </View>
          <View style={{height: this.state.keyboardSpace}}></View>
        </View>
      </ScrollView>
    );
  }
});

module.exports = EmailSignup;