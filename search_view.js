'use strict';

var React = require('react-native');
var FilterView = require('./filter_view');
var PictureView = require('./picture_view');
var Dimensions = require('Dimensions');
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var {
	StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  Image,
} = React;

var styles = StyleSheet.create({
  searchView: {
  },
  searchGrid: {
    marginTop: 55,
    flexDirection: 'column',
  },
  sgRow: {
    flexDirection: 'row',
  },
  sgItemLeft: {
    flex: 1,
    height: screenWidth/2,
    width: screenWidth/2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sgItemRight: {
    flex: 1,
    height: screenWidth/2,
    width: screenWidth/2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  sgItemLeftI: {
    width: screenWidth/2,
    height: screenWidth/2,
    borderWidth: 0.5,
    borderColor: '#999',
  },
  sgItemRightI: {
    width: screenWidth/2,
    height: screenWidth/2,
    borderWidth: 0.5,
    borderColor: '#999',
  },
});

var SearchResult = React.createClass({
  getDefaultProps: function() {
    return {
      image: 'http://placehold.it/400x400&text=D',
    };
  },
  onBook: function() {
    global.navigator.push({
      name: 'PictureView',
      component: PictureView,
    });
  },
  render: function() {
    return (
      <TouchableWithoutFeedback onPress={this.onBook}>
        <View style={styles.sgItemLeft}>
          <Image
            style={styles.sgItemLeftI}
            source={{uri: this.props.image}} />
        </View>
      </TouchableWithoutFeedback>
    );
  }
});

var SearchView = React.createClass({
  render: function() {
    var results = [];
    this.props.serviceResults.map(function(result) {
      console.log('Result: ' + result.img);
      var tmp = <SearchResult image={result.img} />
      results.push(tmp);
    })
    return (
      <View style={styles.searchView}>
        <ScrollView style={styles.searchGrid}>
          <View style={styles.sgRow}>
            {results}
          </View>
          <View style={styles.sgRow}>
            <View style={styles.sgItemLeft}>
                <Image
                  style={styles.sgItemLeftI}
                  source={{uri: 'http://placehold.it/400x400&text=D'}}/>
            </View>
            <View style={styles.sgItemRight}>
                <Image
                  style={styles.sgItemRightI}
                  source={{uri: 'http://placehold.it/400x400&text=D'}}/>
            </View>
          </View>
          <View style={styles.sgRow}>
            <View style={styles.sgItemLeft}>
                <Image
                  style={styles.sgItemLeftI}
                  source={{uri: 'http://placehold.it/400x400&text=D'}}/>
            </View>
            <View style={styles.sgItemRight}>
                <Image
                  style={styles.sgItemRightI}
                  source={{uri: 'http://placehold.it/400x400&text=D'}}/>
            </View>
          </View>
        </ScrollView>
        <FilterView/>
      </View>
    );
  }
});

module.exports = SearchView;