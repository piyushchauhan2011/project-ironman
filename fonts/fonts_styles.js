'use strict';

var React = require('react-native');

var {
	StyleSheet,
} = React;

var styles = StyleSheet.create({
  lato: {
    fontFamily: 'Lato-Regular',
  },
});

module.exports = styles;