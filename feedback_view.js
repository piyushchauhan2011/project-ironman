'use strict';

var React = require('react-native');

var Dimensions = require('Dimensions');
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var {
	StyleSheet,
  SwitchIOS,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableWithoutFeedback,
} = React;

var styles = StyleSheet.create({
  feedbackView: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#EEE',
  },
  feedbackText: {
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  termsAndConditions: {
    flex: 1,
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  txtTAC: {
    paddingLeft: 10,
    fontSize: 16,
  },
  txtFeedback: {
    height: 100,
    borderRadius: 5,
    backgroundColor: '#FAFAFA',
    borderColor: '#DDD',
    padding: 10,
    margin: 10,
    borderWidth: 2,
    fontSize: 18,
    shadowColor: '#CCC',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.4,
    shadowRadius: 2,
  },
  feedbackInfo: {
    fontSize: 20,
    justifyContent: 'center',
  },
  submitContainer: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
  },
  button: {
    flex: 1,
    height: 36,
    backgroundColor: '#009D6E',
    borderColor: '#008C5D',
    borderWidth: 1,
    borderRadius: 3,
    alignItems: 'stretch',
    alignSelf: 'stretch',
    justifyContent: 'center',
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

var FeedbackView = React.createClass({
  getInitialState: function() {
    return {
      trueSwitchIsOn: true,
      falseSwitchIsOn: false,
    };
  },
  changeSwitchState: function() {
    console.log('Changing');
    this.setState({trueSwitchIsOn: !this.state.trueSwitchIsOn});
  },
  removeBlur: function() {
    this.refs.FeedbackInput.blur();
  },
  onSubmit: function() {
    console.log('Your feedback has been submitted.');
  },
  render: function() {
    return (
      <ScrollView style={styles.feedbackView}>
        <View style={styles.feedbackText}>
          <Text style={styles.feedbackInfo}>Please send us any feedback or feature requests you have for the app.</Text>
        </View>
        <View style={styles.termsAndConditions}>
          <SwitchIOS
            onValueChange={this.changeSwitchState}
            value={this.state.trueSwitchIsOn} />
          <TouchableWithoutFeedback onPress={this.changeSwitchState}>
            <Text style={styles.txtTAC}>Attach logs for tech issues.</Text>
          </TouchableWithoutFeedback>
        </View>
        <TextInput clearButtonMode='always' ref='FeedbackInput' enablesReturnKeyAutomatically={true} multiline={true} style={styles.txtFeedback}
          placeholder='Enter your feedback here...'/>

        <View style={styles.submitContainer}>
          <TouchableHighlight style={[styles.button,{marginRight: 5}]} underlayColor='#002E20' onPress={this.onSubmit}>
            <Text style={styles.buttonText}>SUBMIT</Text>
          </TouchableHighlight>
          <TouchableHighlight style={[styles.button,{marginLeft: 5}]} underlayColor='#002E20' onPress={this.removeBlur}>
            <Text style={styles.buttonText}>CANCEL</Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
});

module.exports = FeedbackView;