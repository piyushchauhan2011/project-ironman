'use strict';

var React = require('react-native');
var Booking = require('./booking.js');
var Icon = require('FAKIconImage');
var fonts = require('./fonts/fonts_styles');
var moment = require('moment');
var _ = require('lodash');
var DocitStore = require('./docit_store');

var Dimensions = require('Dimensions');
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var {
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableHighlight,
  Image,
  PickerIOS,
  DatePickerIOS,
} = React;

var PickerItemIOS = PickerIOS.Item;

var styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    backgroundColor: '#F3F3F3',
    padding: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  phImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  phDetails: {
    flex: 3,
    flexDirection: 'column',
  },
  phLocation: {
    flexDirection: 'row',
    marginTop: 5,
  },
  phlIcon: {
    flex: 1,
    height: 25,
    width: 25,
    padding: 5,
  },
  phlText: {
    flex: 8,
    padding: 5,
  },
  apptImage: {
    height: 200,
    flex: 1,
    backgroundColor: '#DFD',
  },
  socialRow: {
    flexDirection: 'row',
    backgroundColor: '#DED',
  },
  socialIcon: {
    flex: 1,
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom: 5,
  },
  apptDetails: {
    flexDirection: 'column',
    backgroundColor: '#EEE',
  },
  apptNameV: {
    flex: 1,
    padding: 5,
  },
  apptName: {
    fontSize: 16,
  },
  tags: {
    flex: 1,
    flexDirection: 'row',
  },
  tag: {
    backgroundColor: '#291028',
    borderRadius: 3,
    padding: 5,
    margin: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  tagName: {
    color: '#FFF',
    fontSize: 14,
  },
  apptComments: {
    flexDirection: 'column',
    backgroundColor: '#EEE',
  },
  apptHeading: {
    flex: 1,
    padding: 5,
  },
  acHeading: {
    fontSize: 18,
  },
  apptComment: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#EFE',
  },
  commentorNameV: {
    flex: 1,
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom: 5,
  },
  commentorName: {
    fontSize: 16,
  },
  commentorText: {
    flex: 1,
    paddingLeft: 20,
  },
  btnClickContain: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    backgroundColor: '#009D6E',
    borderRadius: 5,
    padding: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    borderRadius: 10,
  },
  btnIcon: {
    height: 25,
    width: 25,
  },
  btnCircle: {
    height: 50,
    width: 50,
    borderWidth: 2,
    borderColor: '#AAA',
    borderRadius: 25,
  },
  btnText: {
    fontSize: 18,
    color: '#FAFAFA',
    marginLeft: 10,
    marginTop: 2,
  },
  apptCalendar: {
    flex: 1,
    flexDirection: 'column',
  },
  selectContainer: {
    flex: 1,
    flexDirection: 'row',
    padding: 5,
  },
  typeIcon: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  typeName: {
    flex: 9,
    padding: 5,
  },
  calendarView: {
    flex: 1,
    flexDirection: 'column',
  },
  cvControlRow: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#009D6E',
    shadowColor: '#333',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
  },
  cvDateChooser: {
    flex: 6,
    padding: 5,
    paddingLeft: 10,
  },
  cvDCName: {
    fontSize: 18,
    color: '#FFF',
  },
  cvNPButton: {
    flex: 3,
    flexDirection: 'row',
    padding: 3,
  },
  cvNextAv: {
    flex: 5,
    padding: 5,
  },
  cvNAName: {
    fontSize: 18,
    color: '#FFF',
  },
  calendarPicker: {
    flex: 1,
    flexDirection: 'row',
  },
  cpCol: {
    flex: 1,
    flexDirection: 'column',
  },
  cpcHead: {
    flex: 1,
    backgroundColor: '#FAFAFA',
    padding: 5,
    shadowColor: '#222',
    shadowOffset: { width: 1, height: 5, },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  cpchText: {
    fontSize: 16,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  cpcBookings: {
    backgroundColor: '#DDD',
    height: 200,
  },
  cpcbButtons: {
    padding: 5,
  },
  cpcbBtnContainer: {
    padding: 5,
    backgroundColor: '#FAFAFA',
    borderRadius: 2,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  cpcbBtnText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#333',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  pictureViewPicker: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: '#AA233A',
    shadowColor: '#999',
    shadowOffset: { width: 0, height: -2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    flex: 1,
    width: screenWidth,
  },
  closePicker: {
    flex: 1,
    backgroundColor: '#FAFAFA',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    justifyContent: 'center',
    padding: 10,
  },
});

var PictureHeader = React.createClass({
  getInitialState: function() {
    return {};
  },
  render: function() {
    return (
      <View style={styles.header}>
        <View style={styles.phImage}>
          <Icon
            name='fontawesome|briefcase'
            size={25}
            color='#042'
            style={styles.btnCircle}/>
        </View>
        <View style={styles.phDetails}>
          <View style={styles.phLocation}>
            <Icon
              name='fontawesome|building-o'
              size={25}
              color='#402'
              style={styles.phlIcon}/>
            <Text style={styles.phlText}>{this.props.businessDetails.name}</Text>
          </View>

          <View style={styles.phLocation}>
            <Icon
              name='fontawesome|map-marker'
              size={25}
              color='#042'
              style={styles.phlIcon}/>
            <Text style={styles.phlText}>{this.props.businessDetails.state}</Text>
          </View>
        </View>
      </View>
    );
  }
});

var ApptDetails = React.createClass({
  render: function() {
    return (
      <View style={styles.apptDetails}>
        <View style={styles.apptNameV}>
          <Text style={styles.apptName}>Appointment Name</Text>
        </View>
        <View style={styles.tags}>
          <View style={styles.tag}><Text style={styles.tagName}>#hair</Text></View>
          <View style={styles.tag}><Text style={styles.tagName}>#2015</Text></View>
          <View style={styles.tag}><Text style={styles.tagName}>#style</Text></View>
        </View>
      </View>
    );
  }
});

var SocialRow = React.createClass({
  render: function() {
    return (
      <View style={styles.socialRow}>
        <View style={styles.socialIcon}>
          <Icon
            name='fontawesome|cogs'
            size={25}
            color='#042'
            style={styles.phlIcon}/>
        </View>
        <View style={styles.socialIcon}>
          <Icon
            name='fontawesome|heart'
            size={25}
            color='#042'
            style={styles.phlIcon}/>
        </View>
      </View>
    );
  }
});

var ApptComments = React.createClass({
  render: function() {
    return (
      <View style={styles.apptComments}>
        <View style={styles.apptHeading}><Text style={styles.acHeading}>Comments</Text></View>
        <View style={styles.apptComment}>
          <View style={styles.commentorNameV}>
            <Text style={styles.commentorName}>Andrew Nada</Text>
          </View>
          <View style={styles.commentorText}>
            <Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, nihil officia iste repudiandae similique alias ipsa necessitatibus mollitia libero tempore rerum unde, corrupti. Ratione itaque sed nobis in, deserunt illo!
            </Text>
          </View>
        </View>
        <View style={styles.apptComment}>
          <View style={styles.commentorNameV}>
            <Text style={styles.commentorName}>John Smith</Text>
          </View>
          <View style={styles.commentorText}>
            <Text>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, nihil officia iste repudiandae similique alias ipsa necessitatibus mollitia libero tempore rerum unde, corrupti. Ratione itaque sed nobis in, deserunt illo!
            </Text>
          </View>
        </View>
      </View>
    );
  }
});

var ImageView = React.createClass({
  getDefaultProps: function() {
    return {
      service: {
        img: 'http://placehold.it/400x400&text=D',
      }
    }
  },
  render: function() {
    return (
      <View style={styles.apptImage}>
        <Image
        style={{ width: 400, height: 200 }}
        source={{uri: this.props.service.img}}/>
      </View>
    );
  }
});

var CalendarPlugin = React.createClass({
  fetchTimeslots: function(startingDate, serviceId, employmentId, range) {
    var todaysDate = startingDate;
    var start_date = todaysDate.format('YYYY-MM-DD');
    var service_id = serviceId;
    var employment_id = employmentId;
    var range = range; // default is 4
    var data_string = 'start_date='+start_date+'&range='+range+'&service_id='+service_id+'&employment_id='+employment_id;

    var firstSlots = [];
    var secondSlots = [];
    var thirdSlots = [];
    var fourthSlots = [];
    fetch('http://localhost:3000/timeslots?'+data_string, {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => {
      var data = {};
      data.text = response.text();
      data.status = data.text._32;
      data.value = JSON.parse(data.text._8);

      // Refactor the logic in future, right now doing the dirty way.
      if(data.value[0] !== undefined) {
        var tSlot;
        var tmp_current = moment(data.value[0][0].start_time);
        var diff = tmp_current.date() - todaysDate.date();
        if(diff === 0) { tSlot = firstSlots; }
        if(diff === 1) { tSlot = secondSlots; }
        if(diff === 2) { tSlot = thirdSlots; }
        if(diff === 3) { tSlot = fourthSlots; }

        data.value[0].forEach(function(value) {
          tSlot.push(value.start_time);
        });
      }
      if(data.value[1] !== undefined) {
        var tSlot;
        var tmp_current = moment(data.value[1][0].start_time);
        var diff = tmp_current.date() - todaysDate.date();
        if(diff === 0) { tSlot = firstSlots; }
        if(diff === 1) { tSlot = secondSlots; }
        if(diff === 2) { tSlot = thirdSlots; }
        if(diff === 3) { tSlot = fourthSlots; }

        data.value[1].forEach(function(value) {
          tSlot.push(value.start_time);
        });
      }
      if(data.value[2] !== undefined) {
        var tSlot;
        var tmp_current = moment(data.value[2][0].start_time);
        var diff = tmp_current.date() - todaysDate.date();
        if(diff === 0) { tSlot = firstSlots; }
        if(diff === 1) { tSlot = secondSlots; }
        if(diff === 2) { tSlot = thirdSlots; }
        if(diff === 3) { tSlot = fourthSlots; }

        data.value[2].forEach(function(value) {
          tSlot.push(value.start_time);
        });
      }
      if(data.value[3] !== undefined) {
        var tSlot;
        var tmp_current = moment(data.value[3][0].start_time);
        var diff = tmp_current.date() - todaysDate.date();
        if(diff === 0) { tSlot = firstSlots; }
        if(diff === 1) { tSlot = secondSlots; }
        if(diff === 2) { tSlot = thirdSlots; }
        if(diff === 3) { tSlot = fourthSlots; }

        data.value[3].forEach(function(value) {
          tSlot.push(value.start_time);
        });
      }
      // upto here refactor

      this.setState({
        firstSlots: firstSlots,
        secondSlots: secondSlots,
        thirdSlots: thirdSlots,
        fourthSlots: fourthSlots,
      });
    })
    .catch((error) => {
      console.warn(error);
    });
  },
  // refactor this plugin into its own seperate app components linked together as a glue for separation of concerns.
  getInitialState: function() {
    global.CLPlugin = this;
    var calToday = moment();
    // Only way to add and subtract as a new object through cloning and retaining the old calendar dates of four days.
    var calTodayP = moment(_.clone(calToday.toString()));
    calTodayP = calTodayP.add(1,'days');
    var calTodayPP = moment(_.clone(calTodayP.toString()));
    calTodayPP = calTodayPP.add(1,'days');
    var calTodayPPP = moment(_.clone(calTodayPP.toString()));
    calTodayPPP = calTodayPPP.add(1,'days');
    // end

    var serviceId = 1;
    var employmentId = 2;
    var range = 3;

    this.fetchTimeslots(calToday, serviceId, employmentId, range);

    return {
      current: calToday.toString(),
      firstDate: calToday.format('ddd').toUpperCase() + ' ' + calToday.format('D') + '/' + calToday.format('M'),
      secondDate: calTodayP.format('ddd').toUpperCase() + ' ' + calTodayP.format('D') + '/' + calTodayP.format('M'),
      thirdDate: calTodayPP.format('ddd').toUpperCase() + ' ' + calTodayPP.format('D') + '/' + calTodayPP.format('M'),
      fourthDate: calTodayPPP.format('ddd').toUpperCase() + ' ' + calTodayPPP.format('D') + '/' + calTodayPPP.format('M'),
      firstSlots: [],
      secondSlots: [],
      thirdSlots: [],
      fourthSlots: [],
    };
  },
  onConfirmBooking: function(tmpDate, slotTime) {
    this.props.navigator.push({
      name: 'Booking',
      component: Booking,
      passProps: {
        bookingDate: tmpDate,
        slotTime: slotTime,
        professional: {
          name: 'Stuart Davis',
        },
        business: {
          name: 'Hair By Me',
        },
        appointment: {
          type: 'Haircut',
        },
      }
    });
  },
  updateCurrent: function(date) {
    this.setState({
      current: date.toString(),
    });
  },
  onPrevious: function() {
    var calToday = moment(this.state.current); calToday = calToday.subtract(4, 'days');

    var serviceId = 1;
    var employmentId = 2;
    var range = 3;

    this.fetchTimeslots(calToday, serviceId, employmentId, range);

    var calPrevious = moment(_.clone(calToday.toString()));
    var calPreviousP = moment(_.clone(calPrevious.toString()));
    calPreviousP = calPreviousP.add(1,'days');
    var calPreviousPP = moment(_.clone(calPreviousP.toString()));
    calPreviousPP = calPreviousPP.add(1,'days');
    var calPreviousPPP = moment(_.clone(calPreviousPP.toString()));
    calPreviousPPP = calPreviousPPP.add(1,'days');

    this.setState({
      current: calToday.toString(),
      firstDate: calPrevious.format('ddd').toUpperCase() + ' ' + calPrevious.format('D') + '/' + calPrevious.format('M'),
      secondDate: calPreviousP.format('ddd').toUpperCase() + ' ' + calPreviousP.format('D') + '/' + calPreviousP.format('M'),
      thirdDate: calPreviousPP.format('ddd').toUpperCase() + ' ' + calPreviousPP.format('D') + '/' + calPreviousPP.format('M'),
      fourthDate: calPreviousPPP.format('ddd').toUpperCase() + ' ' + calPreviousPPP.format('D') + '/' + calPreviousPPP.format('M'),
    });
  },
  setFourDates: function(date) {
    var calToday = moment(date);
    
    var calNext = moment(_.clone(calToday.toString()));
    var calNextP = moment(_.clone(calNext.toString()));
    calNextP = calNextP.add(1,'days');
    var calNextPP = moment(_.clone(calNextP.toString()));
    calNextPP = calNextPP.add(1,'days');
    var calNextPPP = moment(_.clone(calNextPP.toString()));
    calNextPPP = calNextPPP.add(1,'days');

    this.setState({
      firstDate: calNext.format('ddd').toUpperCase() + ' ' + calNext.format('D') + '/' + calNext.format('M'),
      secondDate: calNextP.format('ddd').toUpperCase() + ' ' + calNextP.format('D') + '/' + calNextP.format('M'),
      thirdDate: calNextPP.format('ddd').toUpperCase() + ' ' + calNextPP.format('D') + '/' + calNextPP.format('M'),
      fourthDate: calNextPPP.format('ddd').toUpperCase() + ' ' + calNextPPP.format('D') + '/' + calNextPPP.format('M'),
    });
  },
  onNext: function() {
    var calToday = moment(this.state.current); calToday = calToday.add(4, 'days');

    var serviceId = 1;
    var employmentId = 2;
    var range = 3;

    this.fetchTimeslots(calToday, serviceId, employmentId, range);
    
    var calNext = moment(_.clone(calToday.toString()));
    var calNextP = moment(_.clone(calNext.toString()));
    calNextP = calNextP.add(1,'days');
    var calNextPP = moment(_.clone(calNextP.toString()));
    calNextPP = calNextPP.add(1,'days');
    var calNextPPP = moment(_.clone(calNextPP.toString()));
    calNextPPP = calNextPPP.add(1,'days');

    this.setState({
      current: calToday.toString(),
      firstDate: calNext.format('ddd').toUpperCase() + ' ' + calNext.format('D') + '/' + calNext.format('M'),
      secondDate: calNextP.format('ddd').toUpperCase() + ' ' + calNextP.format('D') + '/' + calNextP.format('M'),
      thirdDate: calNextPP.format('ddd').toUpperCase() + ' ' + calNextPP.format('D') + '/' + calNextPP.format('M'),
      fourthDate: calNextPPP.format('ddd').toUpperCase() + ' ' + calNextPPP.format('D') + '/' + calNextPPP.format('M'),
    });
  },
  showDatePicker: function() {
    console.log('showing the datepicker');
    this.props.showPicker('datepicker');
  },
  render: function() {
    // Bad code logic should be written outside of render.
    var calToday = moment(this.state.current);
    var calFour = moment(_.clone(calToday.toString()));
    calFour.add(3,'days');

    var col1Buttons = [];
    this.state.firstSlots.forEach(function(slotTime) {
      var tmpDate = slotTime;
      slotTime = moment(slotTime).format('hh:mm a'); 
      col1Buttons.push (
        <View style={styles.cpcbButtons} key={'col1'+slotTime}>
          <TouchableHighlight onPress={this.onConfirmBooking.bind(this, tmpDate, slotTime)} style={styles.cpcbBtnContainer} underlayColor='#EEE'>
            <Text style={styles.cpcbBtnText}>{slotTime}</Text>
          </TouchableHighlight>
        </View>
      );
    }.bind(this));
    var col2Buttons = [];
    this.state.secondSlots.forEach(function(slotTime) {
      var tmpDate = slotTime;
      slotTime = moment(slotTime).format('hh:mm a');  
      col2Buttons.push (
        <View style={styles.cpcbButtons} key={'col2'+slotTime}>
          <TouchableHighlight onPress={this.onConfirmBooking.bind(this, tmpDate, slotTime)} style={styles.cpcbBtnContainer} underlayColor='#EEE'>
            <Text style={styles.cpcbBtnText}>{slotTime}</Text>
          </TouchableHighlight>
        </View>
      );
    }.bind(this));
    var col3Buttons = [];
    this.state.thirdSlots.forEach(function(slotTime) {
      var tmpDate = slotTime;
      slotTime = moment(slotTime).format('hh:mm a');
      col3Buttons.push (
        <View style={styles.cpcbButtons} key={'col3'+slotTime}>
          <TouchableHighlight onPress={this.onConfirmBooking.bind(this, tmpDate, slotTime)} style={styles.cpcbBtnContainer} underlayColor='#EEE'>
            <Text style={styles.cpcbBtnText}>{slotTime}</Text>
          </TouchableHighlight>
        </View>
      );
    }.bind(this));
    var col4Buttons = [];
    this.state.fourthSlots.forEach(function(slotTime) {
      var tmpDate = slotTime;
      slotTime = moment(slotTime).format('hh:mm a');
      col4Buttons.push (
        <View style={styles.cpcbButtons} key={'col4'+slotTime}>
          <TouchableHighlight onPress={this.onConfirmBooking.bind(this, tmpDate, slotTime)} style={styles.cpcbBtnContainer} underlayColor='#EEE'>
            <Text style={styles.cpcbBtnText}>{slotTime}</Text>
          </TouchableHighlight>
        </View>
      );
    }.bind(this));

    return (
      <View style={styles.calendarView}>
        <View style={styles.cvControlRow}>
          <TouchableHighlight style={styles.cvDateChooser} onPress={this.showDatePicker} underlayColor='#006B4B'>
            <Text style={[styles.cvDCName, fonts.lato]}>
              <Text>{calToday.format('MMMM')}, </Text>
              <Text>{calToday.format('D')}-{calFour.format('D')} </Text>
              <Text>{calToday.format('gggg')}</Text>
            </Text>
          </TouchableHighlight>
          <View style={styles.cvNPButton}>
            <TouchableHighlight onPress={this.onPrevious} underlayColor='#049764'>
            <Icon
              name='fontawesome|angle-left'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
            </TouchableHighlight>
            <TouchableHighlight onPress={this.onNext} underlayColor='#049764'>
            <Icon
              name='fontawesome|angle-right'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
            </TouchableHighlight>
          </View>
          <View style={styles.cvNextAv}>
            <Text style={styles.cvNAName}>Next Available</Text>
          </View>
        </View>
        <View style={styles.calendarPicker}>
          <View style={styles.cpCol}>
            <View style={styles.cpcHead}>
              <Text style={[styles.cpchText, fonts.lato]}>{this.state.firstDate}</Text>
            </View>
            <ScrollView style={styles.cpcBookings}>
              {col1Buttons}
            </ScrollView>
          </View>
          <View style={styles.cpCol}>
            <View style={styles.cpcHead}>
              <Text style={[styles.cpchText, fonts.lato]}>{this.state.secondDate}</Text>
            </View>
            <ScrollView style={styles.cpcBookings}>
              {col2Buttons}
            </ScrollView>
          </View>
          <View style={styles.cpCol}>
            <View style={styles.cpcHead}>
              <Text style={[styles.cpchText, fonts.lato]}>{this.state.thirdDate}</Text>
            </View>
            <ScrollView style={styles.cpcBookings}>
              {col3Buttons}
            </ScrollView>
          </View>
          <View style={styles.cpCol}>
            <View style={styles.cpcHead}>
              <Text style={[styles.cpchText, fonts.lato]}>{this.state.fourthDate}</Text>
            </View>
            <ScrollView style={styles.cpcBookings}>
              {col4Buttons}
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
});
// End of Calendar Plugin to be refactored in a library component later

var CalendarView = React.createClass({
  showAppointmentPicker: function() {
    this.props.showPicker('appointment');
  },
  showProfessionalPicker: function() {
    this.props.showPicker('professional');
  },
  render: function() {
    return (
      <View style={styles.apptCalendar}>
        <View style={styles.selectContainer}>
          <View style={styles.typeIcon}>
            <Icon
              name='fontawesome|wrench'
              size={25}
              color='#402'
              style={styles.phlIcon}/>
          </View>
          <TouchableHighlight onPress={this.showAppointmentPicker} style={styles.typeName} underlayColor='#EEE'>
            <Text>Appointments Type Picker</Text>
          </TouchableHighlight>
        </View>
        <View style={styles.selectContainer}>
          <View style={styles.typeIcon}>
            <Icon
              name='fontawesome|user'
              size={25}
              color='#402'
              style={styles.phlIcon}/>
          </View>
          <TouchableHighlight onPress={this.showProfessionalPicker} style={styles.typeName} underlayColor='#EEE'>
            <Text>Professionals Picker</Text>
          </TouchableHighlight>
        </View>
        <CalendarPlugin navigator={this.props.navigator} showPicker={this.props.showPicker}/>
      </View>
    );
  }
});

var APPOINTMENT_TYPES = {
  first: { name: 'First' },
  second: { name: 'Second' },
  third: { name: 'Third' },
  fourth: { name: 'Fourth' }
};

var ShowPicker = React.createClass({
  getDefaultProps: function () {
    return {
      date: new Date(),
      timeZoneOffsetInHours: (-1) * (new Date()).getTimezoneOffset() / 60,
    };
  },
  getInitialState: function() {
    return {
      appointmentType: 'first',
      appointmentIndex: 3,
      date: this.props.date,
      timeZoneOffsetInHours: this.props.timeZoneOffsetInHours,
    };
  },
  someChange: function(appointmentType) {
    this.setState({
      appointmentType: appointmentType, appointmentIndex: 0
    });
  },
  renderPickerItem: function(appointmentType) {
    return (
      <PickerItemIOS
        key={appointmentType}
        value={appointmentType}
        label={APPOINTMENT_TYPES[appointmentType].name}/>
    );
  },
  onDateChange: function(date) {
    this.setState({date: date});
    // console.log('Changed: ' + date);
    // console.log('Changed: ' + moment(date).toString());
    global.CLPlugin.setState({
      current: date.toString(),
    });
    global.CLPlugin.fetchTimeslots(moment(date), 1, 2, 3);
    global.CLPlugin.setFourDates(date);
  },
  onTimezoneChange: function(event) {
    var offset = parseInt(event.nativeEvent.text, 10);
    if (isNaN(offset)) {
      return;
    }
    this.setState({timeZoneOffsetInHours: offset});
  },
  closePicker: function() {
    this.props.closePicker();
  },
  render: function() {
    var fragment = (
      <TouchableHighlight style={styles.closePicker} underlayColor='#DDD' onPress={this.closePicker}>
        <Text style={{ fontSize: 20, alignItems: 'center', alignSelf: 'center', justifyContent: 'center', }}>Close</Text>
      </TouchableHighlight>
    );
    var pickerType;
    if(this.props.name === 'appointment') {
      pickerType = (
        <PickerIOS 
          selectedValue={this.state.appointmentType}
          onValueChange={this.someChange}>
            {Object.keys(APPOINTMENT_TYPES).map(this.renderPickerItem)}
        </PickerIOS>
      );
    } else if(this.props.name === 'datepicker') {
      pickerType = (
        <DatePickerIOS
          date={this.state.date}
          timeZoneOffsetInMinutes={this.state.timeZoneOffsetInHours * 60}
          onDateChange={this.onDateChange}
          mode="date"/>
      );
    } else {
      fragment = null;
      pickerType = null;
    }
    return (
      <View style={styles.pictureViewPicker}>
        {fragment}
        {pickerType}
      </View>
    );
  }
});

var PictureView = React.createClass({
  fetchServices: function() {
    // Refactor this code to store for better efficieny and code management
    var impHeaders = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };
    if(DocitStore.loggedIn) {
      impHeaders = DocitStore.getImpHeaders();
    }
    var business_id = this.props.businessDetails.id;
    fetch('http://localhost:3000/businesses/'+business_id+'/services', {
      method: 'get',
      headers: impHeaders,
    }).then((response) => {
      var responseString = response.text();
      var data = JSON.parse(responseString._8);
      var status = JSON.parse(responseString._32);
      this.setState({
        services: data,
      });
    }).catch((error) => {
      console.warn(error);
    });
  },
  getInitialState: function() {
    this.fetchServices();
    return {
      showImageView: true,
      showCalendarView: false,
      services: [],
    };
  },
  changeTo: function() {
    this.setState({
      showImageView: !this.state.showImageView,
      showCalendarView: !this.state.showCalendarView,
    });
  },
  showPicker: function(pickerType) {
    this.setState({
      picker: pickerType,
    });
  },
  closePicker: function() {
    this.setState({
      picker: 'blank'
    });
  },
  render: function() {
    var fragment = <ImageView service={this.props.service}/>;
    var changeButton;
    if(this.state.showCalendarView === true) {
      fragment = <CalendarView navigator={this.props.navigator} showPicker={this.showPicker}/>;
      changeButton = (
        <TouchableHighlight
          onPress={this.changeTo} style={styles.btnClickContain}
          underlayColor='#042417'>
          <View
            style={styles.btnContainer}>
            <Icon
              name='fontawesome|picture-o'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
            <Text style={styles.btnText}>Picture</Text>
          </View>
        </TouchableHighlight>
      );
    } else {
      fragment = <ImageView service={this.props.service}/>;
      changeButton = (
        <TouchableHighlight
          onPress={this.changeTo} style={styles.btnClickContain}
          underlayColor='#042417'>
          <View
            style={styles.btnContainer}>
            <Icon
              name='fontawesome|calendar'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
            <Text style={styles.btnText}>Book</Text>
          </View>
        </TouchableHighlight>
      );
    }
    var picker = <ShowPicker name={this.state.picker} closePicker={this.closePicker}/>;
    return (
      <View style={{ flex: 1, height: screenWidth, position: 'relative' }}>
      <ScrollView>
        <PictureHeader businessDetails={this.props.businessDetails}/>
        {fragment}
        <View style={{ padding: 5 }}>
          {changeButton}
        </View>
        <ApptDetails/>
      </ScrollView>
        {picker}
      </View>
    );
  }
});

module.exports = PictureView;