'use strict';

var React = require('react-native');
var PictureView = require('./picture_view');
var Icon = require('FAKIconImage');
var DocitStore = require('./docit_store');

var {
  StyleSheet,
  Text,
  TextInput,
  View,
  ScrollView,
  TouchableHighlight,
  Image,
} = React;

var styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    backgroundColor: '#F3F3F3',
    padding: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  phImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  phDetails: {
    flex: 3,
    flexDirection: 'column',
  },
  phLocation: {
    flexDirection: 'row',
    marginTop: 5,
  },
  phlIcon: {
    flex: 1,
    height: 25,
    width: 25,
    padding: 5,
  },
  phlText: {
    flex: 8,
    padding: 5,
  },
  btnCircle: {
    height: 50,
    width: 50,
    borderWidth: 2,
    borderColor: '#AAA',
    borderRadius: 25,
  },
  mapView: {
    flex: 1,
    flexDirection: 'column',
  },
  mapImage: {
    width: 400,
    height: 200,
  },
  bookButtonV: {
    flex: 1,
    flexDirection: 'column',
  },
  bookButton: {
    backgroundColor: '#009D6E',
    padding: 10,
    shadowColor: '#333',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
  },
  bookBText: {
    fontSize: 24,
    color: '#FFF',
    marginLeft: 10,
  },
  appointmentPictures: {
    flex: 1,
    flexDirection: 'row',
  },
  baPictureV: {
    flex: 1,
  },
  baPicture: {
    height: 100,
    width: 100,
    borderWidth: 0.8,
    borderColor: '#999',
  },
  btnIcon: {
    height: 25,
    width: 25,
  },
  btnContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    borderRadius: 10,
  },
});

var PictureHeader = React.createClass({
  render: function() {
    return (
      <View style={styles.header}>
        <View style={styles.phImage}>
          <Icon
            name='fontawesome|briefcase'
            size={25}
            color='#042'
            style={styles.btnCircle}/>
        </View>
        <View style={styles.phDetails}>
          <View style={styles.phLocation}>
            <Icon
              name='fontawesome|building-o'
              size={25}
              color='#402'
              style={styles.phlIcon}/>
            <Text style={styles.phlText}>{this.props.businessName}</Text>
          </View>

          <View style={styles.phLocation}>
            <Icon
              name='fontawesome|map-marker'
              size={25}
              color='#042'
              style={styles.phlIcon}/>
            <Text style={styles.phlText}>{this.props.businessLocation}</Text>
          </View>
        </View>
      </View>
    );
  }
});

var MapView = React.createClass({
  render: function() {
    return (
      <View style={styles.mapView}>
        <Image
          style={styles.mapImage}
          source={{uri: 'https://maps.googleapis.com/maps/api/staticmap?center=Berkeley,CA&zoom=14&size=400x400'}} />
      </View>
    );
  }
});

// Keep track of the businesses variable
var BookButton = React.createClass({
  onBook: function() {
    global.navigator.push({
      name: 'PictureView',
      component: PictureView,
      passProps: {
        businessDetails: this.props.businessDetails,
      },
    });
  },
  render: function() {
    return (
      <View style={styles.bookButtonV}>
        <TouchableHighlight
          style={styles.bookButton}
          underlayColor='#00412D'
          onPress={this.onBook}>
          <View style={styles.btnContainer}>
          <Icon
            name='fontawesome|calendar'
            size={25}
            color='#063'
            style={styles.btnIcon}/>
          <Text style={styles.bookBText}>Book</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
});

var Pictures = React.createClass({
  getDefaultProps: function() {
    return {
      service: {
        img: 'http://placehold.it/100x100&text=D'
      }
    }
  },
  onServiceBooking: function() {
    global.navigator.push({
      name: 'PictureView',
      component: PictureView,
      passProps: {
        service: this.props.service,
        businessDetails: this.props.businessDetails,
      },
    });
  },
  render: function() {
    return (
      <TouchableHighlight style={styles.baPictureV} onPress={this.onServiceBooking}>
        <Image style={styles.baPicture}
          source={{uri: this.props.service.img}} />
      </TouchableHighlight>
    );
  }
});

var AppointmentPictures = React.createClass({
  getDefaultProps: function() {
    return {
      services: [],
    };
  },
  render: function() {
    var pictures = [];
    this.props.services.map(function(service) {
      var tmp = <Pictures service={service} businessDetails={this.props.businessDetails}/>
      pictures.push(tmp);
    }.bind(this));
    return (
      <View>
        <View style={styles.appointmentPictures}>
          {pictures}
        </View>
        <View style={styles.appointmentPictures}>
          <Pictures/>
          <Pictures/>
          <Pictures/>
          <Pictures/>
        </View>
      </View>
    );
  }
});

var ListPictureView = React.createClass({
  getInitialState: function() {
    // console.log(this.props.businessDetails);
    var impHeaders = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
    if(DocitStore.loggedIn) {
      impHeaders = DocitStore.getImpHeaders();
    }
    var business_id = this.props.businessDetails.id;
    fetch('http://localhost:3000/businesses/'+business_id+'/services', {
      method: 'get',
      headers: impHeaders,
    }).then((response) => {
      var responseString = response.text();
      var data = JSON.parse(responseString._8);
      var status = JSON.parse(responseString._32);
      this.setState({
        services: data,
      });
    }).catch((error) => {
      console.warn(error);
    });
    return {};
  },
  render: function() {
    return (
      <ScrollView>
        <PictureHeader businessName={this.props.businessDetails.name} businessLocation={this.props.businessDetails.state}/>
        <MapView/>
        <BookButton businessDetails={this.props.businessDetails}/>
        <AppointmentPictures services={this.state.services} businessDetails={this.props.businessDetails}/>
      </ScrollView>
    );
  }
});

module.exports = ListPictureView;