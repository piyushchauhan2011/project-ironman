'use strict';

var React = require('react-native');
var BookingSuccessful = require('./booking_successful');

var {
	StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
} = React;

var styles = StyleSheet.create({
  verificationView: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#EEE',
  },
  verificationText: {
    padding: 10,
  },
  verificationInput: {
    padding: 10,
  },
  viTxtInput: {
    height: 40,
    borderColor: '#999',
    borderRadius: 5,
    borderWidth: 1,
    padding: 10,
  },
  button: {
    height: 36,
    flexDirection: 'row',
    backgroundColor: '#009D6E',
    borderColor: '#008C5D',
    borderWidth: 1,
    borderRadius: 3,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    margin: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  agreeTerms: {
    padding: 10,
  },
});

var VerificationView = React.createClass({
  onVConfirm: function() {
    this.props.navigator.push({
      name: 'BookingSuccessful',
      component: BookingSuccessful,
    });
  },
  render: function() {
    return (
      <View style={styles.verificationView}>
        <View style={styles.verificationText}>
          <Text>Hi Sally, A 4 digit code has been sent to you, please enter below to verify your account & confirm your booking.
          </Text>
        </View>
        <View style={styles.verificationInput}>
          <TextInput style={styles.viTxtInput} />
        </View>
        <View style={styles.agreeTerms}>
          <Text>Put a checkbox here</Text>
          <Text>Agree to T & C</Text>
        </View>
        <TouchableHighlight style={styles.button} onPress={this.onVConfirm} underlayColor='#002E20'>
          <Text style={styles.buttonText}>Verify & Confirm</Text>
        </TouchableHighlight>
      </View>
    );
  }
});

module.exports = VerificationView;