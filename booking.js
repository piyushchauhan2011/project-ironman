'use strict';

var React = require('react-native');
var BookingSuccessful = require('./booking_successful');
var Icon = require('FAKIconImage');
var moment = require('moment');
var DocitStore = require('./docit_store');

var {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableHighlight,
  Image,
} = React;

var styles = StyleSheet.create({
  confirmationCard: {
    flexDirection: 'column',
    backgroundColor: '#FAFAFA',
    borderRadius: 1,
    borderColor: '#CCC',
    borderWidth: 1,
    margin: 10,
    padding: 10,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  professionalDetails: {
    flexDirection: 'row',
  },
  pdImageV: {
    flex: 2,
  },
  pdImage: {
    height: 100,
    width: 100,
  },
  pdDetails: {
    flex: 4,
    flexDirection: 'column',
  },
  pdName: {
    padding: 5,
  },
  bdName: {
    padding: 5,
  },
  apptType: {
    padding: 5,
  },
  pdNametxt: {
    fontSize: 18,
    color: '#444',
  },
  bdNametxt: {
    fontSize: 18,
    color: '#444',
  },
  apptTypetxt: {
    fontSize: 18,
    color: '#444',
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 5,
    marginBottom: 5,
  },
  txtLabel: {
    fontSize: 16,
    color: '#333',
    margin: 5,
  },
  txtInput: {
    height: 30,
    borderColor: '#CCC',
    borderRadius: 2,
    borderWidth: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
  },
  staticDetails: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 5,
  },
  sdIcon: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  sdText: {
    flex: 10,
    paddingTop: 3,
    paddingLeft: 5,
  },
  sdtTxt: {
    fontSize: 16,
  },
  bookingButtons: {
    flexDirection: 'row',
    padding: 10,
    marginTop: 10,
  },
  button: {
    height: 36,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#009D6E',
    borderColor: '#008C5D',
    borderWidth: 1,
    borderRadius: 3,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    margin: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  phlIcon: {
    flex: 1,
    height: 25,
    width: 25,
    padding: 5,
  },
});

var Booking = React.createClass({
  getFullName: function() {
    return this.state.user.first_name + ' ' + this.state.user.last_name;
  },
  getInitialState: function() {
    var user = DocitStore.getLoggedUser();
    console.log(user);
    return {
      user: user,
    };
  },
  onConfirm: function() {
    var bookingDetails = {};
    bookingDetails.name = this.getFullName();
    bookingDetails.email = this.state.user.email;
    bookingDetails.phone = this.state.user.phone;
    bookingDetails.notes = this.state.consumerNotes;
    bookingDetails.bookingDate = this.props.bookingDate;
    bookingDetails.slotTime = this.props.slotTime;
    bookingDetails.professional = this.props.professional;
    bookingDetails.business = this.props.professional;
    bookingDetails.appointment = this.props.professional;

    // load booking successful view
    this.props.navigator.push({
      name: 'BookingSuccessful',
      component: BookingSuccessful,
      passProps: {
        bookingDetails: bookingDetails,
      }
    });
  },
  onCancel: function() {
    console.log(global.header);
  },
  render: function() {
    return (
      <View>
        <View style={styles.confirmationCard}>
          <View style={styles.professionalDetails}>
            <View style={styles.pdImageV}>
              <Image style={styles.pdImage}
                source={{ uri: 'http://placehold.it/100x100&text=D'}}/>
            </View>
            <View style={styles.pdDetails}>
              <View style={styles.pdName}>
                <Text style={styles.pdNametxt}>{this.props.professional.name}</Text>
              </View>
              <View style={styles.bdName}>
                <Text style={styles.bdNametxt}>{this.props.business.name}</Text>
              </View>
              <View style={styles.apptType}>
                <Text style={styles.apptTypetxt}>{this.props.appointment.type}</Text>
              </View>
            </View>
          </View>
          <View style={styles.appointmentDetails}>
            <View style={styles.inputContainer}>
              <Text style={styles.txtLabel}>Name</Text>
              <TextInput editable={false}
                style={styles.txtInput} placeholder='Your good name...'
                value={this.getFullName.call(this)} />
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.txtLabel}>Email</Text>
              <TextInput editable={false}
                style={styles.txtInput} placeholder='Specify your email address'
                value={this.state.user.email} />
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.txtLabel}>Contact Number</Text>
              <TextInput editable={false}
                style={styles.txtInput} placeholder='Contact number...'
                value={this.state.user.phone} />
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.txtLabel}>Enter notes here</Text>
              <TextInput 
                style={styles.txtInput} placeholder='Specific Notes...'
                onChangeText={(text) => this.setState({consumerNotes: text})} />
            </View>
            <View style={{ marginTop: 10 }}>
              <View style={styles.staticDetails}>
                <View style={styles.sdIcon}>
                  <Icon
                    name='fontawesome|location-arrow'
                    size={25}
                    color='#402'
                    style={styles.phlIcon}/>
                </View>
                <View style={styles.sdText}>
                  <Text style={styles.sdtTxt}>{this.props.slotTime}</Text>
                </View>
              </View>
              <View style={styles.staticDetails}>
                <View style={styles.sdIcon}>
                  <Icon
                    name='fontawesome|clock-o'
                    size={25}
                    color='#402'
                    style={styles.phlIcon}/>
                </View>
                <View style={styles.sdText}>
                  <Text style={styles.sdtTxt}>{moment(this.props.bookingDate).format('Do MMMM')} ( {moment(this.props.bookingDate).fromNow()} )</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.bookingButtons}>
          <TouchableHighlight onPress={this.onCancel} style={styles.button} underlayColor='#002E20'>
            <Text style={styles.buttonText}>Cancel</Text>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.onConfirm} style={styles.button} underlayColor='#002E20'>
            <Text style={styles.buttonText}>Confirm</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
});

module.exports = Booking;