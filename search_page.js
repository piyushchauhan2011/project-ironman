'use strict';

var React = require('react-native');
var SearchPageDetails = require('./search_page_details');
var DocitStore = require('./docit_store');

var {
  StyleSheet,
  View,
  ScrollView,
  TextInput,
  Text,
  TouchableHighlight,
  ActivityIndicatorIOS,
} = React;

var styles = StyleSheet.create({
  searchView: {
    flexDirection: 'column',  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchResult: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FEFEFE',
    borderBottomWidth: 1,
    borderBottomColor: '#EEE',
    padding: 20,
  },
  searchItem: {
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    fontSize: 20,
  }
});

var SearchResult = React.createClass({
  onSearchPressed: function() {
    global.header.setState({ other: true });
    global.navigator.push({
      name: 'SearchPageDetails',
      component: SearchPageDetails,
      passProps: {
        details: this.props.details
      }
    });
  },
  render: function() {
    return (
      <TouchableHighlight
        style={styles.searchResult}
        onPress={this.onSearchPressed} underlayColor='#EEE'>
        <Text style={styles.searchItem}>{this.props.name}</Text>
      </TouchableHighlight>
    );
  }
});

var SearchPage = React.createClass({
  getInitialState: function() {
    // global.searchPage = this;
    var tmp = <SearchResult details={[{}]} name={'Piyush Chauhan'} />;
    return {
      animating: false,
      results: tmp,
    };
  },
  populateAndSearchResults: function(text) {
    var impHeaders = {};
    if(DocitStore.loggedIn) {
      impHeaders = DocitStore.getImpHeaders();
    }
    this.setState({
      animating: true,
    });
    fetch('http://localhost:3000/search_b?q='+text, {
      method: 'get',
      headers: impHeaders,
    }).then((response) => {
      var responseString = response.text();
      var data = JSON.parse(responseString._8);
      var status = JSON.parse(responseString._32);
      var results = [];
      data.forEach(function(value) {
        var tmp = <SearchResult details={value} name={value.name} />;
        results.push(tmp);
      });
      this.setState({
        results: results,
        animating: false,
      });
    }).catch((error) => {
      console.warn(error);
    });
  },
  render: function() {
    return (
      <ScrollView style={styles.searchView}>
        {this.state.results}
        <ActivityIndicatorIOS
          animating={this.state.animating}
          style={[styles.centering, {height: 80}]}
          size="large" />
      </ScrollView>
    );
  }
});

module.exports = SearchPage;