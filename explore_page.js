'use strict';

var React = require('react-native');

var {
	StyleSheet,
  Text,
  View,
} = React;

var styles = StyleSheet.create({
});

var ExplorePage = React.createClass({
  render: function() {
    return (
      <View>
        <Text>Hello! {this.props.appointmentType} , {this.props.appointmentIndex}</Text>
      </View>
    );
  }
});

module.exports = ExplorePage;