'use strict';

var React = require('react-native');
var PictureView = require('./picture_view');
var Icon = require('FAKIconImage');
var fonts = require('./fonts/fonts_styles');
var DocitStore = require('./docit_store');
var WelcomePage = require('./welcome_page');

var {
  Text,
  TextInput,
  StyleSheet,
  View,
  SegmentedControlIOS,
  ScrollView,
  TouchableHighlight,
  TouchableWithoutFeedback,
} = React;

var styles = StyleSheet.create({
  appointmentTabs: {
    margin: 10,
  },
  appointmentView: {
    backgroundColor: '#EEE',
  },
  upcomingCard: {
    backgroundColor: '#FEFEFE',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    padding: 10,
    borderWidth: 1,
    borderColor: '#DDD',
    borderRadius: 3,
    shadowColor: '#999',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  businessStyle: {
    flexDirection: 'row',
    position: 'relative',
  },
  bsImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    paddingLeft: 5,
  },
  bsDetails: {
    flex: 5,
    flexDirection: 'column',
    padding: 10,
  },
  bsName: {
    fontSize: 18,
  },
  bsLocation: {
    fontSize: 18,
  },
  bsContainer: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 5,
  },
  bsText: {
    flex: 5,
    flexDirection: 'column',
    padding: 5,
    fontSize: 16,
  },
  button: {
    height: 36,
    flexDirection: 'row',
    backgroundColor: '#009D6E',
    borderColor: '#008C5D',
    borderWidth: 1,
    borderRadius: 3,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
    margin: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  btnIcon: {
    flex: 1,
    height: 25,
    width: 25,
    padding: 5,
  },
  btnIconLarge: {
    flex: 1,
    height: 50,
    width: 50,
    padding: 5,
    borderWidth: 2,
    borderColor: '#CCC',
    borderRadius: 25,
  },
  cancellationPolicy: {
    padding: 10,
  },
  cpText: {
    fontSize: 18,
  },
  flipButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    height: 25,
    width: 25,
  },
});

var UpcomingFront = React.createClass({
  onFlip: function() {
    this.props.parentRef.setState({
      frontCardShow: false,
      backCardShow: true,
    });
  },
  render: function() {
    return (
      <View style={styles.upcomingCard}>
        <View style={styles.businessStyle}>
          <View style={styles.bsImage}>
            <Icon
              name='fontawesome|user'
              size={50}
              color='#042'
              style={styles.btnIconLarge}/>
          </View>
          <View style={styles.bsDetails}>
            <Text style={styles.bsName}>Nods Laser Clinic</Text>
            <Text style={styles.bsLocation}>Strathfield</Text>
          </View>
          <TouchableWithoutFeedback onPress={this.onFlip}>
            <Icon
              name='fontawesome|dot-circle-o'
              size={25}
              color='#042'
              style={styles.flipButton}/>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.bsContainer}>
          <Icon
              name='fontawesome|wrench'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
          <Text style={styles.bsText}>Appoinment Type</Text>
        </View>
        <View style={styles.bsContainer}>
          <Icon
            name='fontawesome|calendar-o'
            size={25}
            color='#042'
            style={styles.btnIcon}/>
          <Text style={styles.bsText}>Date</Text>
        </View>
        <View style={styles.bsContainer}>
          <Icon
            name='fontawesome|clock-o'
            size={25}
            color='#042'
            style={styles.btnIcon}/>
          <Text style={styles.bsText}>Time</Text>
        </View>
        <View style={styles.bsContainer}>
          <Icon
            name='fontawesome|file-text-o'
            size={25}
            color='#042'
            style={styles.btnIcon}/>
          <Text style={styles.bsText}>Notes</Text>
        </View>
      </View>
    );
  }
});

var UpcomingBack = React.createClass({
  onFlip: function() {
    this.props.parentRef.setState({
      frontCardShow: true,
      backCardShow: false,
    });
  },
  onSchedule: function() {
    global.header.setState({ other: true });
    global.navigator.push({
      name: 'PictureView',
      component: PictureView,
    });
  },
  render: function() {
    return (
      <View style={styles.upcomingCard}>
        <TouchableHighlight style={styles.button} underlayColor='#002E20' onPress={this.onFlip}>
          <Text style={styles.buttonText}>Cancel</Text>
        </TouchableHighlight>
        <View style={styles.cancellationPolicy}>
          <Text style={styles.cpText}>Note: [Nads Laser Clinics] cancellation policy will apply.</Text>
        </View>
        <TouchableHighlight style={styles.button} underlayColor='#002E20' onPress={this.onSchedule}>
          <Text style={styles.buttonText}>Schedule</Text>
        </TouchableHighlight>
      </View>
    );
  }
});

var Upcoming = React.createClass({
  getInitialState: function() {
    return {
      frontCardShow: true,
      backCardShow: false,
    };
  },
  render: function() {
    var fragment = <UpcomingFront/>;
    if(this.state.frontCardShow === true) {
      fragment = <UpcomingFront parentRef={this}/>;
    }
    if(this.state.backCardShow === true) {
      fragment = <UpcomingBack parentRef={this}/>;
    }

    return (
      <View>
        {fragment}
      </View>
    );
  }
});

var Previous = React.createClass({
  reBook: function() {
    global.header.setState({ other: true });
    global.navigator.push({
      name: 'PictureView',
      component: PictureView,
    });
    // this.props.parentRef.setState({ fragmentId: 'explore' });
  },
  render: function() {
    return (
      <View style={styles.upcomingCard}>
        <View style={styles.businessStyle}>
          <View style={styles.bsImage}>
            <Icon
              name='fontawesome|user'
              size={50}
              color='#042'
              style={styles.btnIconLarge}/>
          </View>
          <View style={styles.bsDetails}>
            <Text style={styles.bsName}>Nods Laser Clinic</Text>
            <Text style={styles.bsLocation}>Strathfield</Text>
          </View>
        </View>
        <View style={styles.bsContainer}>
          <Icon
              name='fontawesome|wrench'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
          <Text style={styles.bsText}>Appoinment Type</Text>
        </View>
        <View style={styles.bsContainer}>
          <Icon
            name='fontawesome|file-text-o'
            size={25}
            color='#042'
            style={styles.btnIcon}/>
          <Text style={styles.bsText}>Notes</Text>
        </View>
        <TouchableHighlight underlayColor='#002E20' onPress={this.reBook} style={styles.button}>
          <Text style={styles.buttonText}>Book Again</Text>
        </TouchableHighlight>
      </View>
    );
  }
});

var Appointments = React.createClass({
  getInitialState: function() {
    DocitStore.callWhenLoggedIn(function() {
      global.header.setState({ other: false });
      var user = DocitStore.getLoggedUser();
      var impHeaders = DocitStore.getImpHeaders();
      fetch('http://localhost:3000/businesses/1/services/1/employments', {
        method: 'get',
        headers: impHeaders
      })
      .then((response) => {
        console.log(response.headers);
        var responseString = response.text();
        console.log(responseString);
        console.log(response);
        var responseText = responseString._8;
        var statusCode = responseString._32;
        console.log(responseText);
      })
      .catch((error) => {
        console.warn(error);
      });
    });
    return {
      previousShow: false,
      upcomingShow: true,
    };
  },
  _onValueChange: function(value) {
    if(value === 'Previous') {
      this.setState({
        previousShow: true,
        upcomingShow: false,
      });
    } else if(value === 'Upcoming') {
      this.setState({
        previousShow: false,
        upcomingShow: true,
      });
    }
  },
  render: function() {
    var fragment = <Upcoming/>;
    if(this.state.upcomingShow === true) {
      fragment = (
        <View>
        <Upcoming/>
        <Upcoming/>
        <Upcoming/>
        <Upcoming/>
        </View>
      );
    }

    if(this.state.previousShow === true) {
      fragment = (
        <View>
          <Previous parentRef={this.props.parentRef} />
          <Previous parentRef={this.props.parentRef} />
          <Previous parentRef={this.props.parentRef} />
        </View>
      );
    }

    return (
      <ScrollView style={styles.appointmentView}>
        <SegmentedControlIOS
          style={[styles.appointmentTabs, fonts.lato]}
          tintColor="#009D3E"
          values={['Upcoming', 'Previous']}
          onValueChange={this._onValueChange}
          selectedIndex={0} />
        <View>
          {fragment}
        </View>
      </ScrollView>
    );
  }
});

module.exports = Appointments;