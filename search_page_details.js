'use strict';

var React = require('react-native');
var Dimensions = require('Dimensions');
var FilterView = require('./filter_view');
var ListPictureView = require('./list_picture_view');
var SearchView = require('./search_view');
var Icon = require('FAKIconImage');
var DocitStore = require('./docit_store');

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var {
  StyleSheet,
  View,
  TextInput,
  ListView,
  Text,
  ScrollView,
  TouchableHighlight,
} = React;

var styles = StyleSheet.create({
  searchView: {
    paddingTop: 0,
    backgroundColor: '#FAFAFA',
    height: screenHeight,
    position: 'relative',
  },
  list: {
    backgroundColor: '#DDD',
    height: screenHeight,
    width: screenWidth,
    position: 'absolute',
    top: 0,
    left: 0,
    paddingTop: 60,
  },
  listItem: {
    backgroundColor: '#FFF',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 3,
    shadowColor: '#999',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  listItemI: {
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    borderRadius: 3,
  },
  spdImage: {
    flex: 1,
    height: 50,
    width: 50,
    margin: 10,
  },
  spdInfo: {
    flex: 3,
    padding: 10,
  },
  spdiRow: {
    flexDirection: 'row',
  },
  spdiIcon: {
    flex: 1,
    margin: 5,
  },
  spdiText: {
    flex: 5,
    margin: 5,
  },
  spdBook: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#009D6E',
    borderColor: '#008C5D',
    borderWidth: 1,
    borderRadius: 5,
    paddingTop: 6,
    paddingBottom: 6,
    marginRight: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center'
  },
  btnIcon: {
    height: 50,
    width: 50,
    borderWidth: 2,
    borderColor: '#AAA',
    borderRadius: 25,
  },
  btnIconSmall: {
    height: 25,
    width: 25,
  }
});

var BusinessResult = React.createClass({
  onBook: function() {
    global.navigator.push({
      name: 'ListPictureView',
      component: ListPictureView,
      passProps: {
        businessDetails: this.props.businessDetails,
      }
    });
  },
  render: function() {
    return (
      <TouchableHighlight style={styles.listItem} onPress={this.onBook} underlayColor='#FAFAFA'>
      <View style={styles.listItemI}>
        <View style={styles.spdImage}>
          <Icon
            name='fontawesome|picture-o'
            size={25}
            color='#042'
            style={styles.btnIcon}/>
        </View>
        <View style={styles.spdInfo}>
          <View style={styles.spdiRow}>
            <Icon
              name='fontawesome|briefcase'
              size={25}
              color='#042'
              style={styles.btnIconSmall}/>
            <Text style={styles.spdiText}>{this.props.businessDetails.name}</Text>
          </View>
          <View style={styles.spdiRow}>
            <Icon
              name='fontawesome|map-marker'
              size={25}
              color='#042'
              style={styles.btnIconSmall}/>
            <Text style={styles.spdiText}>{this.props.businessDetails.state}</Text>
          </View>
        </View>
      </View>
      </TouchableHighlight>
    );
  }
});

var SearchPageDetails = React.createClass({
  populateAndSearchResults: function(text) {
    var impHeaders = {};
    if(DocitStore.loggedIn) {
      impHeaders = DocitStore.getImpHeaders();
    }
    this.setState({
      animating: true,
    });
    fetch('http://localhost:3000/search_s?q='+text, {
      method: 'get',
      headers: impHeaders,
    }).then((response) => {
      var responseString = response.text();
      var data = JSON.parse(responseString._8);
      var status = JSON.parse(responseString._32);
      this.setState({
        serviceResults: data,
        animating: false,
      });
    }).catch((error) => {
      console.warn(error);
    });
    fetch('http://localhost:3000/search_b?q='+text, {
      method: 'get',
      headers: impHeaders,
    }).then((response) => {
      var responseString = response.text();
      var data = JSON.parse(responseString._8);
      var status = JSON.parse(responseString._32);
      console.log(data);
      var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.setState({
        dataSource: ds.cloneWithRows(data)
      });
    }).catch((error) => {
      console.warn(error);
    });
  },
  getInitialState: function() {
    global.searchPage = this;
    // Get all the services
    
    // Get all the businesses
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return {
      fragmentId: 'heart',
      serviceResults: [],
      dataSource: ds.cloneWithRows([
        {
          img: 'http://placehold.it/100x100&text=D',
          name: 'Hair By Me',
          location: 'Redfern'
        },
      ]),
    };
  },

  renderRow: function(rowData, sectionID, rowID) {
    return (
      <BusinessResult businessDetails={rowData} />
    );
  },
  render: function() {
    var fragment = (
      <ScrollView style={styles.list}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderRow}/>
      </ScrollView>
    );
    if(this.state.fragmentId === 'heart') {
      fragment = (
        <SearchView navigator={this.props.navigator} serviceResults={this.state.serviceResults}/>
      );
    } else if(this.state.fragmentId === 'list') {
      fragment = (
        <ScrollView style={styles.list}>
          <ListView
            dataSource={this.state.dataSource}
            renderRow={this.renderRow}/>
          </ScrollView>
      );
    }
    return (
      <View style={styles.searchView}>
        {fragment}
        <FilterView parentRef={this}/>
      </View>
    );
  }
});

module.exports = SearchPageDetails;