'use strict';

var React = require('react-native');
var Dimensions = require('Dimensions');
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;
var Icon = require('FAKIconImage');

var {
	StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
} = React;

var styles = StyleSheet.create({
  filters: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: '#F9F9F9',
    borderBottomColor: '#CCC',
    borderBottomWidth: 1,
    flexDirection: 'row',
    width: screenWidth,
    padding: 10,
    shadowColor: '#BBB',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  ftsSort: {
    flex: 1,
    fontSize: 18,
  },
  ftsDetails: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  ftsViews: {
    flex: 2,
    flexDirection: 'row',
  },
  ftsView: {
    padding: 5,
    borderWidth: 1,
    borderColor: '#E5E5E5',
  },
  ftsItem: {
    padding: 5,
    borderWidth: 1,
    borderColor: '#E5E5E5',
  },
  btnIcon: {
    height: 25,
    width: 25,
  }
});

var FilterView = React.createClass({
  changeToListView: function() {
    this.props.parentRef.setState({ fragmentId: 'list' });
  },
  changeToHeartView: function() {
    this.props.parentRef.setState({ fragmentId: 'heart' });
  },
  render: function() {
    return (
      <View style={styles.filters}>
        <Text style={styles.ftsSort}>SORT</Text>
        <View style={styles.ftsDetails}>
          <Text style={styles.ftsItem}>Filter 1</Text>
          <Text style={styles.ftsItem}>Filter 2</Text>
        </View>
        <View style={styles.ftsViews}>
          <TouchableHighlight style={styles.ftsView} onPress={this.changeToListView} underlayColor='#00B981'>
          <Icon
            name='fontawesome|list'
            size={25}
            color='#042'
            style={styles.btnIcon}/>
          </TouchableHighlight>
          <TouchableHighlight style={styles.ftsView} onPress={this.changeToHeartView} underlayColor='#00B981'>
          <Icon
            name='fontawesome|star'
            size={25}
            color='#042'
            style={styles.btnIcon}/>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
});

module.exports = FilterView;