'use strict';

var React = require('react-native');
var Dimensions = require('Dimensions');
var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var PictureView = require('./picture_view');
var Icon = require('FAKIconImage');
var DocitStore = require('./docit_store');
var _ = require('lodash');

var WelcomePage = require('./welcome_page');

var {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableWithoutFeedback,
  Image,
  AlertIOS,
} = React;

var styles = StyleSheet.create({
  businessItem: {
    flexDirection: 'column',
    backgroundColor: '#FAFAFA',
  },
  businessGrid: {
    flexDirection: 'column',
  },
  bgRow: {
    flexDirection: 'row',
  },
  bgItem: {
    flex: 1,
    height: screenWidth/3,
    width: screenWidth/3,
    borderWidth: 1,
    borderColor: '#AAA',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    backgroundColor: '#F3F3F3',
    padding: 5,
    shadowColor: '#999',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 10,
  },
  phImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  phDetails: {
    flex: 3,
    flexDirection: 'column',
  },
  phLocation: {
    flexDirection: 'row',
    marginTop: 5,
  },
  phlIcon: {
    flex: 1,
    height: 25,
    width: 25,
    padding: 5,
  },
  phlText: {
    flex: 8,
    padding: 5,
  },
  btnCircle: {
    height: 50,
    width: 50,
    borderWidth: 2,
    borderColor: '#AAA',
    borderRadius: 25,
  },
  deleteButton: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: '#AA233A',
    shadowColor: '#999',
    shadowOffset: { width: 0, height: -2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    flex: 1,
    width: screenWidth,
    height: 50,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  }
});

var BusinessHeader = React.createClass({
  render: function() {
    return (
      <View style={styles.header}>
        <View style={styles.phImage}>
          <Icon
            name='fontawesome|briefcase'
            size={25}
            color='#042'
            style={styles.btnCircle}/>
        </View>
        <View style={styles.phDetails}>
          <View style={styles.phLocation}>
            <Icon
              name='fontawesome|building-o'
              size={25}
              color='#402'
              style={styles.phlIcon}/>
            <Text style={styles.phlText}>Business Name</Text>
          </View>

          <View style={styles.phLocation}>
            <Icon
              name='fontawesome|map-marker'
              size={25}
              color='#042'
              style={styles.phlIcon}/>
            <Text style={styles.phlText}>Location</Text>
          </View>
        </View>
      </View>
    );
  }
});

var BusinessItem = React.createClass({
  pressedVeryLong: function() {
    console.log('I pressed very long here.');
    this.props.showDeleteButton();
  },
  render: function() {
    return (
      <View style={styles.businessItem}>
        <BusinessHeader/>
        <View style={styles.businessGrid}>
          <View style={styles.bgRow}>
            <TouchableWithoutFeedback onLongPress={this.pressedVeryLong}>
              <Image
              style={styles.bgItem}
              source={{uri: 'http://placehold.it/'+screenWidth/3+'x'+screenWidth/3+'&text=D'}}/>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onLongPress={this.pressedVeryLong}>
              <Image
              style={styles.bgItem}
              source={{uri: 'http://placehold.it/'+screenWidth/3+'x'+screenWidth/3+'&text=D'}}/>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onLongPress={this.pressedVeryLong}>
              <Image
              style={styles.bgItem}
              source={{uri: 'http://placehold.it/'+screenWidth/3+'x'+screenWidth/3+'&text=D'}}/>
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.bgRow}>
            <TouchableWithoutFeedback onLongPress={this.pressedVeryLong}>
              <Image
              style={styles.bgItem}
              source={{uri: 'http://placehold.it/'+screenWidth/3+'x'+screenWidth/3+'&text=D'}}/>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onLongPress={this.pressedVeryLong}>
              <Image
              style={styles.bgItem}
              source={{uri: 'http://placehold.it/'+screenWidth/3+'x'+screenWidth/3+'&text=D'}}/>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onLongPress={this.pressedVeryLong}>
              <Image
              style={styles.bgItem}
              source={{uri: 'http://placehold.it/'+screenWidth/3+'x'+screenWidth/3+'&text=D'}}/>
            </TouchableWithoutFeedback>
          </View>

        </View>
      </View>
    );
  }
});

var LikedView = React.createClass({
  fetchLikedAppointments: function() {
    var impHeaders = DocitStore.getImpHeaders();
    DocitStore.callWhenLoggedIn(function() {
      fetch('http://localhost:3000/favourites', {
        method: 'get',
        headers: impHeaders,
      });
    }, function() {
      global.navigator.push({
        name: 'WelcomePage',
        component: WelcomePage,
      });
    });
  },
  getInitialState: function() {
    this.fetchLikedAppointments();
    return {
      showDeleteButton: false,
    };
  },
  showDeleteButton: function() {
    this.setState({
      showDeleteButton: true,
    });
  },
  closeAndDelete: function() {
    this.setState({
      showDeleteButton: false,
    });
  },
  render: function() {
    var deleteButton;
    if(this.state.showDeleteButton) {
      deleteButton = (
        <TouchableHighlight style={styles.deleteButton} underlayColor='#801826' onPress={this.closeAndDelete}>
          <Text style={{ fontSize: 20, color: '#FAFAFA' }}>Delete Button</Text>
        </TouchableHighlight>
      );
    }
    return (
      <View style={{ flex: 1, height: screenWidth, position: 'relative' }}>
      <ScrollView>
        <BusinessItem showDeleteButton={this.showDeleteButton}/>
        <BusinessItem showDeleteButton={this.showDeleteButton}/>
      </ScrollView>
      {deleteButton}
      </View>
    );
  }
});

module.exports = LikedView;