'use strict';

var React = require('react-native');
var ExplorePage = require('./explore_page');

var {
	View,
  Text,
  PickerIOS,
  StyleSheet,
} = React;

var PickerItemIOS = PickerIOS.Item;

var styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: '#DDD',
  },
  business: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: '#EEE',
  },
  bImage: {
    flex: 1,
  },
  bDetails: {
    flex: 3,
    flexDirection: 'column',
  },
  bApptType: {
    flexDirection: 'column',
  },
  bDName: {},
  bDLocation: {}
});

var APPOINTMENT_TYPES = {
  first: { name: 'First' },
  second: { name: 'Second' },
  third: { name: 'Third' },
  fourth: { name: 'Fourth' }
};

var BookingStep1 = React.createClass({
  getInitialState: function() {
    return {
      appointmentType: 'first',
      appointmentIndex: 3,
    };
  },
  someChange: function(appointmentType) {
    this.setState({
      appointmentType: appointmentType, appointmentIndex: 0
    });
    this.props.navigator.replace({
      title: 'Explore View',
      component: ExplorePage,
      passProps: {
        appointmentType: appointmentType,
        appointmentIndex: 0,
      },
    });
  },
  renderPickerItem: function(appointmentType) {
    return (
      <PickerItemIOS
        key={appointmentType}
        value={appointmentType}
        label={APPOINTMENT_TYPES[appointmentType].name}/>
    );
  },
  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.business}>
          <View style={styles.bImage}>
            <Text>Image</Text>
          </View>
          <View style={styles.bDetails}>
            <Text style={styles.bDName}>Business Name</Text>
            <Text style={styles.bDLocation}>Location</Text>
          </View>
        </View>
        <View style={styles.bApptType}>
          <PickerIOS 
            selectedValue={this.state.appointmentType}
            onValueChange={this.someChange}>
              
              {Object.keys(APPOINTMENT_TYPES).map(this.renderPickerItem)}
          </PickerIOS>
        </View>
      </View>
    );
  }
});

module.exports = BookingStep1;