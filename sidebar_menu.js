'use strict';

var React = require('react-native');
var Dimensions = require('Dimensions');
var ExplorePage = require('./explore_page');
var WelcomePage = require('./welcome_page');
var Appointments = require('./appointments');
var SettingsView = require('./settings_view');
var FeedbackView = require('./feedback_view');
var LikedView = require('./liked');
var Icon = require('FAKIconImage');

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

var {
  StyleSheet,
  View,
	ScrollView,
  Text,
  TouchableHighlight,
} = React;

var styles = StyleSheet.create({
  container: {
    width: screenWidth*2/3,
    height: screenHeight,
    backgroundColor: '#FAFAFA',
    paddingTop: 20,
    position: 'relative',
    flexDirection: 'column',
  },
  menus: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderTopColor: '#EEE',
    borderTopWidth: 1,
  },
  loggedStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  sideBarFooter: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    flexDirection: 'column',
    width: screenWidth*2/3,
  },
  btnIcon: {
    height: 25,
    width: 25,
  },
});

var SideBarMenu = React.createClass({
  clearHeader: function() {
    if(global.header.refs.search) {
      global.header.refs.search.blur();
    }
  },
  liked: function() {
    global.navigator.push({
      name: 'Liked',
      component: LikedView
    });
    // this.props.parentRef.setState({fragmentId: 'view'});
    this.props.menuActions.close();
    this.clearHeader();
  },
  explore: function() {
    global.navigator.push({
      name: 'WelcomePage',
      component: WelcomePage,
    });
    // this.props.parentRef.setState({ fragmentId: 'explore' });
    this.props.menuActions.close();
    this.clearHeader();
  },
  appointments: function() {
    // this.props.parentRef.setState({ fragmentId: 'appointments'});
    global.navigator.push({
      name: 'Appointments',
      component: Appointments
    });
    this.props.menuActions.close();
    this.clearHeader();
  },
  settings: function() {
    global.navigator.push({
      name: 'Settings',
      component: SettingsView
    });
    // this.props.parentRef.setState({ fragmentId: 'settings' });
    this.props.menuActions.close();
    this.clearHeader();
  },
  feedback: function() {
    global.navigator.push({
      name: 'Feeback',
      component: FeedbackView
    });
    // this.props.parentRef.setState({ fragmentId: 'feedback' });
    this.props.menuActions.close();
    this.clearHeader();
  },
  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.loggedStyle}>
          <Icon
              name='fontawesome|user'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
          <Text>Logged In</Text>
        </View>
        <TouchableHighlight onPress={this.explore} style={styles.menus} underlayColor='#DDD'>
          <View style={styles.loggedStyle}>
          <Icon
            name='fontawesome|search'
            size={25}
            color='#042'
            style={styles.btnIcon}/>
          <Text>Explore</Text>
          </View>
        </TouchableHighlight>
        <TouchableHighlight onPress={this.liked} style={styles.menus} underlayColor='#DDD'>
          <View style={styles.loggedStyle}>
            <Icon
              name='fontawesome|heart'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
          <Text>Liked</Text>
          </View>
        </TouchableHighlight>
        <TouchableHighlight onPress={this.appointments} style={styles.menus} underlayColor='#DDD'>
          <View style={styles.loggedStyle}>
            <Icon
              name='fontawesome|clock-o'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
          <Text>Appointments</Text>
          </View>
        </TouchableHighlight>

        <View style={styles.sideBarFooter}>
          <TouchableHighlight onPress={this.settings} style={styles.menus} underlayColor='#DDD'>
          <View style={styles.loggedStyle}>
            <Icon
              name='fontawesome|cog'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
          <Text>Settings</Text>
          </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.feedback} style={styles.menus} underlayColor='#DDD'>
          <View style={styles.loggedStyle}>
            <Icon
              name='fontawesome|comment'
              size={25}
              color='#042'
              style={styles.btnIcon}/>
          <Text>Feedback</Text>
          </View>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
});

module.exports = SideBarMenu;