'use strict';

var React = require('react-native');

var {
  StyleSheet,
  View,
  Text,
  TextInput,
  SwitchIOS,
} = React;

var styles = StyleSheet.create({
  settingsView: {
    flex: 1,
    backgroundColor: '#EEE',
    padding: 10,
  },
  notifications: {
    fontSize: 24,
    marginBottom: 10,
  },
  activityRow: {
    flexDirection: 'row',
    marginTop: 5,
  },
  item1: { flex: 3, },
  item2: { flex: 1, },
});

var SettingsView = React.createClass({
  getInitialState: function() {
    return {
      truePushSwitchIsOn: true,
      falsePushSwitchIsOn: false,
      trueSMSSwitchIsOn: true,
      falseSMSSwitchIsOn: false,
    };
  },
  render: function() {
    return (
      <View style={styles.settingsView}>
        <Text style={styles.notifications}>Notifications</Text>
        <View style={styles.activityRow}>
          <Text style={styles.item1}>Push</Text>
          <SwitchIOS
          onValueChange={(value) => this.setState({falsePushSwitchIsOn: value})}
          style={{marginBottom: 10}}
          value={this.state.falsePushSwitchIsOn} />
        </View>
        <View style={styles.activityRow}>
          <Text style={styles.item1}>SMS</Text>
          <SwitchIOS
          onValueChange={(value) => this.setState({falseSMSSwitchIsOn: value})}
          style={{marginBottom: 10}}
          value={this.state.falseSMSSwitchIsOn} />
        </View>
      </View>
    );
  }
});

module.exports = SettingsView;