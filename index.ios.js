'use strict';

var React = require('react-native');
var SideMenu = require('react-native-side-menu');
var WelcomePage = require('./welcome_page');
var SideBarMenu = require('./sidebar_menu');
var Header = require('./header');
var SearchView = require('./search_view');
var DocitStore = require('./docit_store');

var {
  AppRegistry,
  StyleSheet,
  Text,
  Component,
  Navigator,
  View,
  TouchableHighlight,
  TextInput,
} = React;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#FFF',
  },
});

var ContentView = React.createClass({
  render: function() {
    return (
      <Navigator
        initialRoute={{name: 'WelcomePage', component: WelcomePage}}
        configureScene={() => {
            return Navigator.SceneConfigs.FadeAndroid;
        }}
        renderScene={(route, navigator) => {
            // count the number of func calls
            // console.log(route, navigator);
            global.navigator = navigator;
            global.items += 1;

            if (route.component) {
                return React.createElement(route.component, { navigator, ...route.passProps });
            }
        }}/>
    );
  }
});

var Docit = React.createClass({
  getInitialState: function() {
    DocitStore.init();
    global.items = 0;
    return {
      fragmentId: 'explore'
    };
  },
  render: function() {
    var menu = <SideBarMenu parentRef={this}/>;
    var fragment = <ContentView/>;

    return (
        <SideMenu menu={menu}>
          <Header parentRef={this}/>
          {fragment}
        </SideMenu>
      );
  }
});

AppRegistry.registerComponent('Docit', () => Docit);
