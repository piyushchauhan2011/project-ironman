'use strict';

var React = require('react-native');
var _ = require('lodash');

var {
  AlertIOS,
} = React;

module.exports = {
	init: function() {
    this.loggedUser = {};
    this.headers = {};
    this.loggedIn = false;
  },
  setLoggedUser: function(user) {
    this.loggedUser = user;
  },
  getLoggedUser: function() {
    return this.loggedUser;
  },
  setHeaders: function(headers) {
    this.headers = headers.map;
  },
  getHeaders: function() {
    return this.headers;
  },
  getImpHeaders: function() {
    var headers = {
      'access-token': this.headers['access-token'],
      'token-type': this.headers['token-type'],
      'expiry': this.headers.expiry,
      'client': this.headers.client,
      'uid': this.headers.uid,
    };
    var sharedHeaders = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
    var impHeaders = _.extend(sharedHeaders, headers);

    if(this.loggedIn) {
      return impHeaders;
    } else {
      return sharedHeaders;
    }
  },
  callWhenLoggedIn: function(callback, notCallback) {
    if(this.loggedIn) {    
      callback();
    } else {
      AlertIOS.alert(
        'Hello Guest!','You are not logged In...',
        [
          {
            text: 'Log In', onPress: notCallback
          }
        ]
      );
    }
  },
};